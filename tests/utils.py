from datetime import date, datetime
from flask_security import url_for_security

BUSINESS_NAME = "tezege"
BUSINESS_LOGO = "1e2b9f33e87b43b181d928c9ea95ec70.jpg"
BUSINESS_ADRESS = "Jl. Soekarno Hatta, Lamreung"
BUSINESS_SUB_DISCTRICT = "Darul Amarah"
BUSINESS_DISTRICT = "Aceh Besar"
BUSINESS_PROVINCE = "Banda Aceh"
BUSINESS_COUNTRY = "Indonesia"
BUSINESS_POSTAL_CODE = "23122"
BUSINESS_PHONE_NUMBER = "082390023422"
BUSINESS_EMAIL = "tezege@mail.com"
BUSINESS_WEBSITE = "http://tezege.com/"
BUSINESS_SOCIAL_MEDIA = [
    {"facebook": "https://facebook.com"},
    {"twitter": "https://twitter.com"},
    {"youtube": "https://youtube.com"},
    {"instagram": "https://instagram.com"},
]
BUSINESS_CREATED_AT = datetime(2021, 1, 19, 12, 10, 15, 15)


ROLE_ADMIN = "Admin"
ROLE_DESC = "Admin roles"

USER_TITLE = "Mr."
USER_EMAIL = "user@test.com"
USER_PASSWORD = "user-password"
USER_FIRTST_NAME = "user"
USER_LAST_NAME = "name"
USER_USERNAME = "username"
USER_GENDER = "Man"
USER_BORNDAY = date.today()
USER_CONTACT = "123456789012"
USER_ADDRESS = "user address"
USER_IDCARD = 1234567890123456
USER_ACCOUNT_NAME = "user name"
USER_ACCOUNT_NUMBER = 1234567890123456
USER_BANK_NAME = "ABC"
USER_BANK_BRANCH = "bank branch"
USER_TAX_ID = "1234567890123456"
USER_ABOUT = "about user"
USER_CREATED_AT = datetime(2021, 1, 20, 10, 58, 10, 55)

CATEGORY_NAME = "Daster"

PRODUCT_PHOTO_FILENAME = "1e2b9f33e87b43b181d928c9ea95ec70.jpg"
PRODUCT_NAME = "daster panjang plong lengan"
PRODUCT_PURCHASE_PRICE = 65000
PRODUCT_SELL_PRICE = 75000
PRODUCT_STOCK = 200
PRODUCT_CREATED_AT = datetime(2021, 1, 20, 10, 58, 10, 55)

SALES_CREATED_AT = datetime(2021, 1, 23, 8, 15, 30, 59)

ITEMS_QTY = 3


def auth_login(app):
    """Login User"""
    return app.post(
        url_for_security("login"),
        data=dict(email=USER_EMAIL, password=USER_PASSWORD),
        follow_redirects=True,
    )
