import pytest
from flask_security.utils import hash_password
from src.flask_sip import create_app
from src.flask_sip import user_datastore
from src.flask_sip.extensions import db as database
from src.flask_sip.mod_bussines_profile.models import BusinessProfile
from src.flask_sip.mod_category.models import ProductCategory
from src.flask_sip.mod_product.models import Product
from src.flask_sip.mod_pos.models import Items, Sales
from tests.utils import (
    BUSINESS_ADRESS,
    BUSINESS_COUNTRY,
    BUSINESS_CREATED_AT,
    BUSINESS_DISTRICT,
    BUSINESS_EMAIL,
    BUSINESS_PHONE_NUMBER,
    BUSINESS_POSTAL_CODE,
    BUSINESS_PROVINCE,
    BUSINESS_SOCIAL_MEDIA,
    BUSINESS_SUB_DISCTRICT,
    BUSINESS_WEBSITE,
    ROLE_ADMIN,
    ROLE_DESC,
    USER_TITLE,
    USER_EMAIL,
    USER_PASSWORD,
    USER_FIRTST_NAME,
    USER_LAST_NAME,
    USER_USERNAME,
    USER_GENDER,
    USER_BORNDAY,
    USER_CONTACT,
    USER_ADDRESS,
    USER_IDCARD,
    USER_ACCOUNT_NAME,
    USER_ACCOUNT_NUMBER,
    USER_BANK_NAME,
    USER_BANK_BRANCH,
    USER_TAX_ID,
    USER_ABOUT,
    USER_CREATED_AT,
    CATEGORY_NAME,
    PRODUCT_NAME,
    PRODUCT_PURCHASE_PRICE,
    PRODUCT_SELL_PRICE,
    PRODUCT_STOCK,
    PRODUCT_CREATED_AT,
    BUSINESS_NAME,
    BUSINESS_ADRESS,
    BUSINESS_SUB_DISCTRICT,
    BUSINESS_DISTRICT,
    BUSINESS_PROVINCE,
    BUSINESS_COUNTRY,
    BUSINESS_POSTAL_CODE,
    BUSINESS_PHONE_NUMBER,
    BUSINESS_EMAIL,
    BUSINESS_WEBSITE,
    BUSINESS_SOCIAL_MEDIA,
    BUSINESS_CREATED_AT,
    ITEMS_QTY,
    SALES_CREATED_AT,
)


@pytest.fixture(scope="module")
def app():
    """Initialize app"""
    app = create_app("testing")

    return app


@pytest.fixture
def db(app, client, request):
    database.drop_all()
    database.create_all()
    database.session.commit()

    def fin():
        database.session.remove()

    request.addfinalizer(fin)
    return database


@pytest.fixture
def business_profile(db):
    business_profile = BusinessProfile(
        business_profile_name=BUSINESS_NAME,
        business_profile_address=BUSINESS_ADRESS,
        business_profile_sub_district=BUSINESS_SUB_DISCTRICT,
        business_profile_district=BUSINESS_DISTRICT,
        business_profile_province=BUSINESS_PROVINCE,
        business_profile_country=BUSINESS_COUNTRY,
        business_profile_postal_code=BUSINESS_POSTAL_CODE,
        business_profile_phone_number=BUSINESS_PHONE_NUMBER,
        business_profile_email=BUSINESS_EMAIL,
        business_profile_website=BUSINESS_WEBSITE,
        business_profile_social_media=BUSINESS_SOCIAL_MEDIA,
        business_profile_created_at=BUSINESS_CREATED_AT,
    )
    business_profile.create()
    return business_profile


@pytest.fixture
def role(db):
    role = user_datastore.create_role(name=ROLE_ADMIN, description=ROLE_DESC)
    return role


@pytest.fixture
def user(db, role):
    user = user_datastore.create_user(
        title=USER_TITLE,
        first_name=USER_FIRTST_NAME,
        last_name=USER_LAST_NAME,
        username=USER_USERNAME,
        email=USER_EMAIL,
        password=hash_password(USER_PASSWORD),
        gender=USER_GENDER,
        bornday=USER_BORNDAY,
        contact=USER_CONTACT,
        address=USER_ADDRESS,
        id_card=USER_IDCARD,
        account_name=USER_ACCOUNT_NAME,
        account_number=USER_ACCOUNT_NUMBER,
        bank_name=USER_BANK_NAME,
        branch=USER_BANK_BRANCH,
        tax_id=USER_TAX_ID,
        about_me=USER_ABOUT,
        active=True,
        confirmed=False,
        created_at=USER_CREATED_AT,
    )
    if role:
        user_datastore.add_role_to_user(user, role)
    return user


@pytest.fixture
def category(db):
    category = ProductCategory(name=CATEGORY_NAME, active=True)
    category.create()
    return category


@pytest.fixture
def product(db, category):
    product = Product(
        product_name=PRODUCT_NAME,
        product_purchase_price=PRODUCT_PURCHASE_PRICE,
        product_selling_price=PRODUCT_SELL_PRICE,
        product_stock=PRODUCT_STOCK,
        product_status=True,
        created_at=PRODUCT_CREATED_AT,
        category_id=category.id,
    )
    product.create()
    return product


@pytest.fixture
def sales(
    db,
    user,
):
    sales = Sales(
        created_at=SALES_CREATED_AT,
        total_qty=ITEMS_QTY,
        total_price=PRODUCT_SELL_PRICE * ITEMS_QTY,
        total_profit=(PRODUCT_SELL_PRICE * ITEMS_QTY) - (PRODUCT_PURCHASE_PRICE * ITEMS_QTY),
        user_id=user.id,
    )
    sales.create()
    return sales


@pytest.fixture
def items(db, product, sales):
    items = Items(
        qty=ITEMS_QTY,
        product_purchase_price=product.product_purchase_price,
        product_selling_price=product.product_selling_price,
        sub_total=product.product_selling_price * ITEMS_QTY,
        sub_profit=(product.product_selling_price * ITEMS_QTY)
        - (product.product_purchase_price * ITEMS_QTY),
        sales_id=sales.sales_id,
        product_id=product.product_id,
    )
    items.create()
    return items
