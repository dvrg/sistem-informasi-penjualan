import pytest
from slugify import slugify
from src.flask_sip.mod_category.models import ProductCategory
from tests.utils import CATEGORY_NAME


def test_model_category(category):
    """
    Given a category model
    When category is created
    Then try to match data
    """
    assert category.name == CATEGORY_NAME
    assert category.slug == slugify(CATEGORY_NAME)
    assert category.active
    assert category.count() == 1


def test_model_category_change_status(category):
    """
    Given a category model
    When category is created
    Then change status of category to False, and try to show data
    """
    assert category.active
    show_data = category.get_all_only_available()
    assert show_data != 0
    category.active = False
    category.update()
    show_data = category.get_all_only_available()
    assert show_data == []


def test_model_category_update(category):
    """
    Given a category model
    When create category
    Then change value data of category and try to match
    """
    EDIT_CATEGORY = "One kit"
    assert category.name == CATEGORY_NAME
    assert category.slug == slugify(CATEGORY_NAME)
    assert category.active
    data = category.get(slugify(CATEGORY_NAME))
    data.name = EDIT_CATEGORY
    data.active = False
    data.update()
    assert not category.name == CATEGORY_NAME
    assert not category.slug == slugify(CATEGORY_NAME)
    assert not category.active
    assert category.name == EDIT_CATEGORY
    assert category.slug == slugify(EDIT_CATEGORY)


def test_model_category_delete(category):
    """
    Given a category data
    When data is created
    Then delete category and try to find
    """
    assert category.name == CATEGORY_NAME
    category.delete(category.slug)
    assert not category.get(slugify(CATEGORY_NAME))
    assert category.count() == 0


@pytest.mark.parametrize("name, active", [("Piyama", False), ("One Kit", True), ("Gamis", True)])
def test_model_category_insert_multiple(name, active, category):
    """
    Given a multiple value category data and model category
    When data is created
    Then try to match data
    """
    category = ProductCategory(name=name, active=active)
    category.create()
    assert category.name == name
    assert category.slug == slugify(name)
    assert category.active == active
