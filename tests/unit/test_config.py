import os
from src.flask_sip import create_app
from src.flask_sip.config import SQLITE_DEV, SQLITE_TEST, SQLITE_PROD


def test_config():
    app = create_app("development")

    assert app.config["SECRET_KEY"] == os.getenv("SECRET_KEY")

    assert app.config["SQLALCHEMY_DATABASE_URI"] == SQLITE_DEV
    assert app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]

    assert app.config["MAIL_SERVER"] == "localhost"
    assert app.config["MAIL_PORT"] == 8025
    assert not app.config["MAIL_USE_TLS"]
    assert not app.config["MAIL_USE_SSL"]

    assert app.config["SECURITY_PASSWORD_SALT"] == os.getenv("SECURITY_PASSWORD_SALT")
    assert app.config["SECURITY_TOKEN_MAX_AGE"] == 900  # 15 Minutes
    assert app.config["SECURITY_CONFIRM_SALT"] == os.getenv("SECURITY_CONFIRM_SALT")
    assert app.config["SECURITY_RESET_SALT"] == os.getenv("SECURITY_RESET_SALT")
    assert app.config["SECURITY_LOGIN_SALT"] == os.getenv("SECURITY_LOGIN_SALT")
    assert app.config["SECURITY_REMEMBER_SALT"] == os.getenv("SECURITY_REMEMBER_SALT")

    assert app.config["DEBUG_TB_ENABLED"]
    assert app.config["DEBUG_TB_INTERCEPT_REDIRECTS"]
    assert app.config["DEBUG_TB_PROFILER_ENABLED"]
    assert app.config["DEBUG_TB_TEMPLATE_EDITOR_ENABLED"]
    assert app.config["DEBUG_TB_HOSTS"] == "/report-visualization/"

    assert not app.config["TESTING"]
