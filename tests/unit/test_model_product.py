import pytest
from datetime import datetime
from slugify import slugify
from src.flask_sip.mod_product.models import Product
from tests.utils import (
    PRODUCT_NAME,
    PRODUCT_PURCHASE_PRICE,
    PRODUCT_SELL_PRICE,
    PRODUCT_STOCK,
    PRODUCT_CREATED_AT,
)


def test_model_product(product, category):
    """
    GIVEN product and category model
    When product and category created
    Then match product with category data
    """
    assert product.product_name == PRODUCT_NAME
    assert product.product_purchase_price == PRODUCT_PURCHASE_PRICE
    assert product.product_selling_price == PRODUCT_SELL_PRICE
    assert product.product_stock == PRODUCT_STOCK
    assert product.created_at == PRODUCT_CREATED_AT
    assert product.product_slug == slugify(PRODUCT_NAME)
    assert product.category_id == category.id
    assert product.count() == 1


def test_model_product_update(product):
    """
    GIVEN product model
    When product created
    Then update value and match new value
    """
    EDIT_PRODUCT_NAME = "Daster pendek rempel"
    EDIT_PRODUCT_PURCHASE_PRICE = 48000
    EDIT_PRODUCT_SELL_PRICE = 65000
    EDIT_PRODUCT_STOCK = 120
    PRODUCT_UPDATED_AT = datetime(2021, 1, 10, 12, 21, 11, 25)
    data = product.get(slugify(PRODUCT_NAME))
    data.product_name = EDIT_PRODUCT_NAME
    data.product_purchase_price = EDIT_PRODUCT_PURCHASE_PRICE
    data.product_selling_price = EDIT_PRODUCT_SELL_PRICE
    data.product_stock = EDIT_PRODUCT_STOCK
    data.updated_at = PRODUCT_UPDATED_AT
    data.product_status = False
    data.update()
    assert not product.product_name == PRODUCT_NAME
    assert not product.product_purchase_price == PRODUCT_PURCHASE_PRICE
    assert not product.product_selling_price == PRODUCT_SELL_PRICE
    assert not product.product_stock == PRODUCT_STOCK
    assert not product.product_status
    assert not product.product_slug == slugify(PRODUCT_NAME)
    assert product.product_name == EDIT_PRODUCT_NAME
    assert product.product_purchase_price == EDIT_PRODUCT_PURCHASE_PRICE
    assert product.product_selling_price == EDIT_PRODUCT_SELL_PRICE
    assert product.product_stock == EDIT_PRODUCT_STOCK
    assert product.created_at == PRODUCT_CREATED_AT
    assert product.updated_at == PRODUCT_UPDATED_AT
    assert product.product_slug == slugify(EDIT_PRODUCT_NAME)


def test_model_product_delete(product):
    """
    Given product model
    When product is created
    Then delete data and try to find
    """
    assert product.get(slugify(PRODUCT_NAME))
    product.delete(product.product_slug)
    assert not product.get(slugify(PRODUCT_NAME))
    assert product.count() == 0


def test_model_product_limit_stock(product):
    """
    Given product model
    When product is created
    Then update stock to 3, so function can detect this product is stock limit
    """
    data = product.get(slugify(PRODUCT_NAME))
    assert data.product_stock == PRODUCT_STOCK
    data.product_stock = 3
    data.update()
    assert product.product_stock != PRODUCT_STOCK
    assert product.get_product_with_limit_stock()


@pytest.mark.parametrize(
    "name, purchase_price, selling_price, stock, status, created_at",
    [
        ("Daster Pendek rempel", 48000, 65000, 440, True, datetime(2021, 1, 12, 10, 30, 10, 30)),
        (
            "Daster panjang plong non lengan",
            53000,
            70000,
            320,
            True,
            datetime(2021, 1, 13, 11, 30, 10, 30),
        ),
        ("Piyama non busui", 59000, 75000, 900, True, datetime(2021, 1, 14, 12, 30, 10, 30)),
        (
            "One set LD 120 cm tunik",
            106000,
            125000,
            300,
            False,
            datetime(2021, 1, 15, 13, 30, 10, 30),
        ),
    ],
)
def test_model_product_insert_multiple(
    name, purchase_price, selling_price, stock, status, created_at, product
):
    """
    Given product model
    When prepare multiple value of product
    Then insert all value of product
    """
    product = Product(
        product_name=name,
        product_purchase_price=purchase_price,
        product_selling_price=selling_price,
        product_stock=stock,
        product_status=status,
        created_at=created_at,
    )
    product.create()
    assert product.product_name == name
    assert product.product_purchase_price == purchase_price
    assert product.product_selling_price == selling_price
    assert product.product_stock == stock
    assert product.created_at == created_at
    assert product.product_slug == slugify(name)
