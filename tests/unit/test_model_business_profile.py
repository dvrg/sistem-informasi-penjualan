from datetime import datetime
from tests.utils import (
    BUSINESS_NAME,
    BUSINESS_ADRESS,
    BUSINESS_SUB_DISCTRICT,
    BUSINESS_DISTRICT,
    BUSINESS_PROVINCE,
    BUSINESS_COUNTRY,
    BUSINESS_POSTAL_CODE,
    BUSINESS_PHONE_NUMBER,
    BUSINESS_EMAIL,
    BUSINESS_WEBSITE,
    BUSINESS_SOCIAL_MEDIA,
    BUSINESS_CREATED_AT,
)


def test_model_business_profile(business_profile):
    """
    Given a business profile model
    When created data
    Then try to match data
    """
    assert business_profile.business_profile_name == BUSINESS_NAME
    assert business_profile.business_profile_address == BUSINESS_ADRESS
    assert business_profile.business_profile_sub_district == BUSINESS_SUB_DISCTRICT
    assert business_profile.business_profile_district == BUSINESS_DISTRICT
    assert business_profile.business_profile_province == BUSINESS_PROVINCE
    assert business_profile.business_profile_country == BUSINESS_COUNTRY
    assert business_profile.business_profile_postal_code == BUSINESS_POSTAL_CODE
    assert business_profile.business_profile_phone_number == BUSINESS_PHONE_NUMBER
    assert business_profile.business_profile_email == BUSINESS_EMAIL
    assert business_profile.business_profile_website == BUSINESS_WEBSITE
    assert business_profile.business_profile_social_media == BUSINESS_SOCIAL_MEDIA
    assert business_profile.business_profile_created_at == BUSINESS_CREATED_AT


def test_model_business_profile_update(business_profile):
    """
    Given a business profile model
    When data is created
    Then update the data and try to match data
    """
    EDIT_BUSINESS_NAME = "tezege edit"
    EDIT_BUSINESS_ADRESS = "address edit"
    EDIT_BUSINESS_SUB_DISCTRICT = "sub district edit"
    EDIT_BUSINESS_DISTRICT = "district edit"
    EDIT_BUSINESS_PROVINCE = "province edit"
    EDIT_BUSINESS_COUNTRY = "country edit"
    EDIT_BUSINESS_POSTAL_CODE = "postal code edit"
    EDIT_BUSINESS_PHONE_NUMBER = "phone number edit"
    EDIT_BUSINESS_EMAIL = "email@edit.com"
    EDIT_BUSINESS_WEBSITE = "http://tezegeedit.com"
    EDIT_BUSINESS_SOCIAL_MEDIA = [
        {"facebook": "https://editfacebook.com"},
        {"twitter": "https://edittwitter.com"},
        {"youtube": "https://edityoutube.com"},
        {"instagram": "https://editinstagram.com"},
    ]
    EDIT_BUSINESS_CREATED_AT = datetime(2021, 1, 21, 10, 30, 35, 25)
    assert business_profile.business_profile_name == BUSINESS_NAME
    assert business_profile.business_profile_address == BUSINESS_ADRESS
    assert business_profile.business_profile_sub_district == BUSINESS_SUB_DISCTRICT
    assert business_profile.business_profile_district == BUSINESS_DISTRICT
    assert business_profile.business_profile_province == BUSINESS_PROVINCE
    assert business_profile.business_profile_country == BUSINESS_COUNTRY
    assert business_profile.business_profile_postal_code == BUSINESS_POSTAL_CODE
    assert business_profile.business_profile_phone_number == BUSINESS_PHONE_NUMBER
    assert business_profile.business_profile_email == BUSINESS_EMAIL
    assert business_profile.business_profile_website == BUSINESS_WEBSITE
    assert business_profile.business_profile_social_media == BUSINESS_SOCIAL_MEDIA
    assert business_profile.business_profile_created_at == BUSINESS_CREATED_AT
    data = business_profile.get()
    data.business_profile_name = EDIT_BUSINESS_NAME
    data.business_profile_address = EDIT_BUSINESS_ADRESS
    data.business_profile_sub_district = EDIT_BUSINESS_SUB_DISCTRICT
    data.business_profile_district = EDIT_BUSINESS_DISTRICT
    data.business_profile_province = EDIT_BUSINESS_PROVINCE
    data.business_profile_country = EDIT_BUSINESS_COUNTRY
    data.business_profile_postal_code = EDIT_BUSINESS_POSTAL_CODE
    data.business_profile_phone_number = EDIT_BUSINESS_PHONE_NUMBER
    data.business_profile_email = EDIT_BUSINESS_EMAIL
    data.business_profile_website = EDIT_BUSINESS_WEBSITE
    data.business_profile_social_media = EDIT_BUSINESS_SOCIAL_MEDIA
    data.business_profile_created_at = EDIT_BUSINESS_CREATED_AT
    data.update()
    assert business_profile.business_profile_name == EDIT_BUSINESS_NAME
    assert business_profile.business_profile_address == EDIT_BUSINESS_ADRESS
    assert business_profile.business_profile_sub_district == EDIT_BUSINESS_SUB_DISCTRICT
    assert business_profile.business_profile_district == EDIT_BUSINESS_DISTRICT
    assert business_profile.business_profile_province == EDIT_BUSINESS_PROVINCE
    assert business_profile.business_profile_country == EDIT_BUSINESS_COUNTRY
    assert business_profile.business_profile_postal_code == EDIT_BUSINESS_POSTAL_CODE
    assert business_profile.business_profile_phone_number == EDIT_BUSINESS_PHONE_NUMBER
    assert business_profile.business_profile_email == EDIT_BUSINESS_EMAIL
    assert business_profile.business_profile_website == EDIT_BUSINESS_WEBSITE
    assert business_profile.business_profile_social_media == EDIT_BUSINESS_SOCIAL_MEDIA
    assert business_profile.business_profile_created_at == EDIT_BUSINESS_CREATED_AT
