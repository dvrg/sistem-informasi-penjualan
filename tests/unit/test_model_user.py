from flask_security.utils import verify_password
from src.flask_sip import db
from src.flask_sip import user_datastore
from tests.utils import (
    USER_TITLE,
    USER_FIRTST_NAME,
    USER_LAST_NAME,
    USER_EMAIL,
    USER_PASSWORD,
    USER_USERNAME,
    USER_GENDER,
    USER_BORNDAY,
    USER_CONTACT,
    USER_ADDRESS,
    USER_IDCARD,
    USER_ACCOUNT_NAME,
    USER_ACCOUNT_NUMBER,
    USER_BANK_NAME,
    USER_BANK_BRANCH,
    USER_TAX_ID,
    USER_ABOUT,
    USER_CREATED_AT,
)


def test_model_user(user, role):
    """
    Given a user model
    When a new user created
    Then check data of name, password are same
    """
    assert user.title == USER_TITLE
    assert user.first_name == USER_FIRTST_NAME
    assert user.last_name == USER_LAST_NAME
    assert user.username == USER_USERNAME
    assert user.email == USER_EMAIL
    assert verify_password(USER_PASSWORD, user.password)
    assert user.gender == USER_GENDER
    assert user.bornday == USER_BORNDAY
    assert user.contact == USER_CONTACT
    assert user.address == USER_ADDRESS
    assert user.id_card == USER_IDCARD
    assert user.account_name == USER_ACCOUNT_NAME
    assert user.account_number == USER_ACCOUNT_NUMBER
    assert user.bank_name == USER_BANK_NAME
    assert user.branch == USER_BANK_BRANCH
    assert user.tax_id == USER_TAX_ID
    assert user.about_me == USER_ABOUT
    assert user.active
    assert not user.confirmed
    assert user.created_at == USER_CREATED_AT
    assert user.roles == [None, role]


def test_model_user_remove_role(user, role):
    """
    Given a user model and role model
    When user and role created
    Then add role to user and check if role has been added
    """
    assert user_datastore.remove_role_from_user(user, role)
    assert user.roles == [None]


def test_model_user_check_active(user):
    """
    Given a new user model
    When a new user created
    Then check status activate of user
    """
    assert user.is_active


def test_model_user_check_role(user, role):
    """
    Given a new user model
    When a new user created
    Then check role is user is none
    """
    assert user.has_role(role)


def test_model_user_deactivate(user):
    """
    Given a new user model
    When a new user created
    Then change status user to deactivated
    """
    assert user_datastore.deactivate_user(user)
