import pytest
from src.flask_sip import user_datastore
from tests.utils import ROLE_ADMIN, ROLE_DESC


def test_model_role(role):
    """
    Given a role model
    When a new role created
    Then check data of name, description are same
    """
    assert role.name == ROLE_ADMIN
    assert role.description == ROLE_DESC


def test_model_role_find_data(role):
    """
    Given a role model
    When a new role created
    Then check to find data to make sure data is insert
    """
    assert user_datastore.find_role(ROLE_ADMIN)


@pytest.mark.parametrize(
    "name, desc",
    [
        ("Cashier", "Cashier role for app"),
        ("Admin", "Admin role for app"),
        ("Developer", "Developer role for app"),
    ],
)
def test_model_role_insert_multiple_role(name, desc, role):
    """
    GIVEN set multiple name and desc of role
    WHEN a new set Role is creted
    THEN check set name, description are defined correctly
    """
    assert user_datastore.find_or_create_role(name=name, description=desc)
    assert user_datastore.find_role(name)
