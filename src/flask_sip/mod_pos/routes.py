from flask import render_template
from flask_security import login_required, roles_accepted
from . import bp
from src.flask_sip.mod_bussines_profile.models import BusinessProfile
from .models import Items, Sales
from flask_babel import _
from flask_breadcrumbs import register_breadcrumb


@bp.route("/")
@login_required
@roles_accepted("Developer", "Admin", "Cashier")
def index():
    bp = BusinessProfile.get()
    return render_template("pos.html", title=_("Point of Sales"), bp=bp)


@bp.route("/transaction")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".transaction", _("View Transaction"))
def transaction():
    sales = Sales.gets()
    return render_template("transactions.html", title=_("View Transaction Data"), sales=sales)


@bp.route("/transaction/<int:id>")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".items_transaction", _("View Items Transaction"))
def items_transaction(id):
    items = Items.get_by_sales(id)
    sales = Sales.get(id)
    return render_template(
        "transactions-items.html",
        title=_("View Items Transaction Data"),
        items=items,
        sales=sales,
    )


@bp.route("/report/profit")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".profit_reports", _("View Profit Report"))
def profit_report():
    profit_reports = Sales.sum_of_all_profit_by_year()
    return render_template(
        "report-profit-by-year.html",
        title=_("View Profit Report"),
        profit_reports=profit_reports,
    )


@bp.route("/report/profit/<int:param>")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".profit_report_by", _("View Reports"))
def profit_report_by(param):
    profit_report_by_month = Sales.sum_of_all_profit_by_month(param)
    return render_template(
        "report-profit-by-month.html",
        title=_("View Profit Report"),
        profit_report_by_month=profit_report_by_month,
    )


@bp.route("/report/profit/<int:year>/<int:month>")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".profit_report_by_day", _("View Reports"))
def profit_report_by_day(year, month):
    profit_report_by_day = Sales.sum_of_all_profit_by_date(year, month)
    return render_template(
        "report-profit.html",
        title=_("View Profit Report"),
        profit_report_by_day=profit_report_by_day,
    )


@bp.route("/report/transaction")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".transaction_reports", _("View Transaction Report"))
def transaction_reports():
    transaction_reports = Sales.transaction_report_all()
    return render_template(
        "report-transaction.html",
        title=_("View Transaction Report"),
        transaction_reports=transaction_reports,
    )


@bp.route("/report/transaction/<int:year>")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".transaction_report_by_year", _("View Transaction Report by Year"))
def transaction_report_by_year(year):
    transaction_reports = Sales.transaction_report_by_year(year)
    return render_template(
        "report-transaction-by-year.html",
        title=_("View Transaction Report"),
        transaction_reports=transaction_reports,
    )


@bp.route("/report/transaction/<int:year>/<int:month>")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".transaction_report_by_month", _("View Transaction Report by Month"))
def transaction_report_by_month(year, month):
    transaction_reports = Sales.transaction_report_by_month(year, month)
    return render_template(
        "report-transaction-by-month.html",
        title=_("View Transaction Report"),
        transaction_reports=transaction_reports,
    )


@bp.route("/report/transaction/<int:year>/<int:month>/<int:day>")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".transaction_report_by_day", _("View Transaction Report by Date"))
def transaction_report_by_day(year, month, day):
    transaction_reports = Sales.transaction_report_by_day(year, month, day)
    return render_template(
        "report-transaction-by-day.html",
        title=_("View Transaction Report"),
        transaction_reports=transaction_reports,
    )
