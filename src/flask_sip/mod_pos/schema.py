from src.flask_sip.extensions import ma
from .models import Sales, Items


class SalesSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Sales
        include_relationships = True
        include_fk = False
        load_instance = True


class ItemsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Items
        include_relationships = False
        include_fk = True
        load_instance = True
