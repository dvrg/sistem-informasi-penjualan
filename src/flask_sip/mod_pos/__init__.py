from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_pos", __name__, template_folder="pos_templates")
default_breadcrumb_root(bp, ".")
from . import routes
