from marshmallow import fields
from src.flask_sip.mod_pos.schema import SalesSchema, ItemsSchema


class Sales(SalesSchema):
    """/sales/create [POST]

    Parameters:
    - price_total
    - qty_total
    """

    price_total = fields.Float(required=True)
    qty_total = fields.Int(required=True)
    profit_total = fields.Float(required=True)
    items = fields.List(fields.Nested(ItemsSchema))


class Items(ItemsSchema):
    """ /sales/<id>/items [POST] """

    qty = fields.Int(required=True)
    product_purchase_price = fields.Float(required=True)
    product_selling_price = fields.Float(required=True)
    product_id = fields.Int(required=True)


def load_data(sales_db_obj):
    from src.flask_sip.mod_pos.schema import SalesSchema

    sales_schema = SalesSchema()

    data = sales_schema.dump(sales_db_obj)

    return data
