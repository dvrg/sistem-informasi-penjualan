from flask_restx import Namespace, fields
from ..product.dto import ProductDto

product = ProductDto.product


class SalesDto:
    api = Namespace("sales", description="Sales related operation")

    items = api.model(
        "Items",
        {
            "product_id": fields.Integer(required=True, description="Sales Items identifier"),
            "qty": fields.Integer(required=True, description="Quantity of purchasing"),
            "product_purchase_price": fields.Float(
                required=True, description="Price of product when customer purchase"
            ),
            "product_selling_price": fields.Float(
                required=True, description="Price of product when sell to customer"
            ),
        },
    )

    sales = api.model(
        "Sales",
        {
            "price_total": fields.Float(required=True, description="Total of Price"),
            "qty_total": fields.Integer(required=True, description="Total of Quantity"),
            "profit_total": fields.Float(required=True, description="Total of Profit"),
            "items": fields.Nested(items, required=True, as_list=True),
        },
    )

    data_resp = api.model(
        "Sales data response",
        {
            "status": fields.Boolean,
            "message": fields.String,
            "sales": fields.Nested(sales),
        },
    )
