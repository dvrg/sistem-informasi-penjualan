from datetime import datetime
from src.flask_sip.extensions import db
from src.flask_sip.mod_product.models import Product
from flask import current_app
from flask_security import current_user
from flask_babel import _

from src.flask_sip.utils import err_resp, message, internal_err_resp
from src.flask_sip.mod_pos.models import Sales, Items
from src.flask_sip.mod_pos.schema import ItemsSchema, SalesSchema

sales_schema = SalesSchema()
items_schema = ItemsSchema()


class SalesService:
    @staticmethod
    def get_sales_data(id):
        """ Get sales data by id """
        sales = Sales.get(id)
        if not sales:
            return err_resp("Sales not found!", "sales_404", 404)

        from .utils import load_data

        try:
            sales_data = load_data(sales)

            resp = message(True, "Sales data sent")
            resp["sales"] = sales_data
            return resp, 200
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def list_sales_data():
        """ Get all sales data """
        sales = Sales.gets()
        if not sales:
            return err_resp("Sales is empty", "product_404", 404)

        from .utils import load_data

        try:
            resp = message(True, "Sales data sent")
            resp["sales"] = [load_data(x) for x in sales]
            return resp, 200
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def post_sales(data):
        """ Post sales data """
        total_qty = data["qty_total"]
        total_price = data["price_total"]
        total_profit = data["profit_total"]
        item_entries = []
        product_update = []

        try:
            new_sales = Sales(
                user_id=current_user.id,
                total_price=total_price,
                total_qty=total_qty,
                total_profit=total_profit,
                created_at=datetime.now(),
            )

            new_sales.create()

            for item in data["items"]:
                new_item = Items(
                    product_id=item["product_id"],
                    qty=item["qty"],
                    product_selling_price=item["product_selling_price"],
                    product_purchase_price=item["product_purchase_price"],
                    sub_total=item["qty"] * item["product_selling_price"],
                    sub_profit=(item["qty"] * item["product_selling_price"])
                    - (item["qty"] * item["product_purchase_price"]),
                    sales_id=new_sales.sales_id,
                )
                item_entries.append(new_item)

                product = Product.get_by_id(item["product_id"])
                product.product_stock -= item["qty"]

                product_update.append(product)

            db.session.add_all(item_entries)
            db.session.add_all(product_update)
            db.session.commit()

            sales_info = sales_schema.dump(new_sales)
            items_info = items_schema.dump(item_entries, many=True)
            resp = message(True, _("Sales has been created"))
            resp["sales"] = sales_info
            resp["sales"]["items"] = items_info

            return resp, 201
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()
