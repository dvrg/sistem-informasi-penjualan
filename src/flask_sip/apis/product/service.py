from flask import current_app
from src.flask_sip.utils import err_resp, message, internal_err_resp
from src.flask_sip.mod_product.models import Product
from flask_images import resized_img_src


class ProductService:
    @staticmethod
    def get_product_data(id):
        """ Get product data by slug """
        product = Product.get_by_id(id)
        if not product:
            return err_resp("Product not found!", "product_404", 404)

        from .utils import load_data

        try:
            product_data = load_data(product)

            resp = message(True, "Product data sent")
            resp["product"] = product_data
            return resp, 200
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def list_product_data():
        """ Get all product data """
        product = Product.get_all_only_available()
        for data in product:
            data.product_photo_filename = resized_img_src(
                data.product_photo_filename,
                width=250,
                height=250,
                mode="crop",
                quality=50,
            )
        if not product:
            return err_resp("Product is empty", "product_204", 204)

        from .utils import load_data

        try:
            product_data = [load_data(x) for x in product]
            resp = message(True, "Product data sent")
            resp["product"] = product_data

            return resp, 200
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()
