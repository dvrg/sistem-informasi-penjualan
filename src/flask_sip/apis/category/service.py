from flask import current_app

from src.flask_sip.utils import err_resp, message, internal_err_resp
from src.flask_sip.mod_category.models import ProductCategory
from src.flask_sip.mod_category.schema import CategorySchema

category_schema = CategorySchema()


class CategoryService:
    @staticmethod
    def get_category_data(id):
        """ Get category data by slug """
        category = ProductCategory.get_by_id(id)
        if not category:
            return err_resp("Category not found!", "category_404", 404)

        from .utils import load_data

        try:
            category_data = load_data(category)

            resp = message(True, "Category data sent")
            resp["category"] = category_data
            return resp, 200
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def list_category_data():
        """ Get category data """
        category = ProductCategory.gets()
        if not category:
            return err_resp("Category not found!", "category_404", 404)

        from .utils import load_data

        try:
            resp = message(True, "Category data sent")
            resp["category"] = [load_data(x) for x in category]
            return resp, 200
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def post_category(data):
        """Post category data"""
        name = data["name"]
        active = data["active"]

        try:
            new_category = ProductCategory(name=name, active=active)

            new_category.create()

            category_info = category_schema.dump(new_category)
            resp = message(True, "Category has been created")
            resp["category"] = category_info

            return resp, 201
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp
