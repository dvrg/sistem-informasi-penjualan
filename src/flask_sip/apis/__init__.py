import os
from flask import Blueprint, current_app
from flask_restx import Api
from .product.controller import api as ns_product
from .category.controller import api as ns_category
from .sales.controller import api as ns_sales


title = os.getenv("APP_NAME") or "Default App"

bp = Blueprint("api", __name__)
authorizations = {"Bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}}

api = Api(
    bp,
    title=title,
    version="1.0",
    description=title + " API Documentation",
    doc="/doc/",
    authorizations=authorizations,
    security="Bearer",
)

api.add_namespace(ns_product, path="/product")
api.add_namespace(ns_category, path="/category")
api.add_namespace(ns_sales, path="/sales")
