from datetime import datetime
import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))

SQLITE_DEV = "sqlite:///" + os.path.join(BASEDIR, "sip_dev.db")
SQLITE_TEST = "sqlite:///" + os.path.join(BASEDIR, "sip_test.db")
SQLITE_PROD = "sqlite:///" + os.path.join(BASEDIR, "sip_prod.db")


class Config:
    """Configuration system"""

    APP_NAME = "Flask-POS"
    APP_DESCRIPTION = "Poinf of sales and stock management application"
    APP_KEYWORD = "Point of Sales, sales information system"
    APP_GOOGLE_ANALYTICS = os.getenv("GOOGLE_ANALYTICS")

    SECRET_KEY = os.getenv("SECRET_KEY")
    LOG_TO_STDOUT = True

    # Upload Folder
    ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg"}
    STATIC_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "static")
    IMAGE_UPLOAD = "img/upload"
    IMAGE_UPLOAD_DIR = os.path.join(STATIC_DIR, IMAGE_UPLOAD)

    # Flask Babel Configuration
    BABEL_DEFAULT_LOCALE = "id"
    BABEL_DEFAULT_TIMEZONE = "UTC"
    LANGUAGES = ["id", "en"]

    # Flask Security Feature Flags
    SECURITY_CONFIRMABLE = False
    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = True
    SECURITY_PASSWORDLESS = False
    SECURITY_CHANGEABLE = True
    SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
    SECURITY_DATETIME_FACTORY = datetime.now
    SECURITY_EMAIL_SENDER = os.getenv("MAIL_DEFAULT_SENDER")

    # Flask Mail
    MAIL_DEFAULT_SENDER = os.getenv("MAIL_DEFAULT_SENDER")

    # Flask Bootstrap Configuration
    BOOTSTRAP_USE_MINIFIED = True
    BOOTSTRAP_SERVE_LOCAL = True
    BOOTSTRAP_LOCAL_SUBDOMAIN = None
    BOOTSTRAP_CDN_FORCE_SSL = True
    BOOTSTRAP_QUERYSTRING_REVVING = True

    # Flask Images
    IMAGES_PATH = ["static/img/upload"]

    # Sentry
    SENTRY_DSN = os.getenv("SENTRY_DSN")

    # Flask Restx
    HTTP_BASIC_AUTH_REALM = True

    # Flask Debug Toolbar
    DEBUG_TB_HOSTS = "/report-visualization/"

    # Flask Talisman
    SELF = "'self'"
    CSP = {
        "default-src": [
            SELF,
            "www.gravatar.com",
        ],
        "style-src": [
            SELF,
            "'unsafe-inline'",
            "fonts.googleapis.com",
            "cdn.jsdelivr.net",
            "unpkg.com",
            "*.bootstrapcdn.com",
        ],
        "font-src": [
            SELF,
            "fonts.googleapis.com",
            "themes.googleusercontent.com",
            "*.gstatic.com",
            "cdn.jsdelivr.net",
        ],
        "frame-src": [
            SELF,
            "www.google.com",
            "www.youtube.com",
        ],
        "script-src": [
            SELF,
            "'unsafe-inline'",
            "'unsafe-eval'",
            "code.jquery.com",
            "ajax.googleapis.com",
            "*.googleanalytics.com ",
            "*.google-analytics.com",
            "cdn.jsdelivr.net",
            "www.google.com",
            "www.gstatic.com",
            "unpkg.com",
            "polyfill.io",
        ],
    }


class DevelopmentConfig(Config):

    # Flask SQLAlchemy Configuration
    SQLALCHEMY_DATABASE_URI = SQLITE_DEV
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # Flask Mail Configuration
    MAIL_SERVER = os.getenv("MAIL_SERVER") or "localhost"
    MAIL_PORT = int(os.getenv("MAIL_PORT") or 8025)
    MAIL_USE_TLS = bool(os.getenv("MAIL_USE_TLS")) or False
    MAIL_USE_SSL = bool(os.getenv("MAIL_USE_SSL")) or False
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")

    # Flask Security Configuration
    SECURITY_PASSWORD_SALT = os.getenv("SECURITY_PASSWORD_SALT")
    SECURITY_TOKEN_MAX_AGE = 900
    SECURITY_CONFIRM_SALT = os.getenv("SECURITY_CONFIRM_SALT")
    SECURITY_RESET_SALT = os.getenv("SECURITY_RESET_SALT")
    SECURITY_LOGIN_SALT = os.getenv("SECURITY_LOGIN_SALT")
    SECURITY_REMEMBER_SALT = os.getenv("SECURITY_REMEMBER_SALT")

    # Flask Debugtoolbar Configuration
    DEBUG_TB_ENABLED = True
    DEBUG_TB_INTERCEPT_REDIRECTS = True
    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_TEMPLATE_EDITOR_ENABLED = True


class TestingConfig(Config):

    TESTING = True

    # Flask SQLAlchemy Configuration
    SQLALCHEMY_DATABASE_URI = SQLITE_TEST
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # Flask Mail Configuration
    MAIL_SERVER = os.getenv("MAIL_SERVER") or "localhost"
    MAIL_PORT = int(os.getenv("MAIL_PORT") or 8025)
    MAIL_USE_TLS = bool(os.getenv("MAIL_USE_TLS")) or False
    MAIL_USE_SSL = bool(os.getenv("MAIL_USE_SSL")) or False
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")

    # Flask Security Configuration
    SECURITY_PASSWORD_SALT = os.getenv("SECURITY_PASSWORD_SALT")
    SECURITY_TOKEN_MAX_AGE = 5
    SECURITY_CONFIRM_SALT = os.getenv("SECURITY_CONFIRM_SALT")
    SECURITY_RESET_SALT = os.getenv("SECURITY_RESET_SALT")
    SECURITY_LOGIN_SALT = os.getenv("SECURITY_LOGIN_SALT")
    SECURITY_REMEMBER_SALT = os.getenv("SECURITY_REMEMBER_SALT")


class ProductionConfig(Config):

    LOG_TO_STDOUT = False

    # Flask SQLAlchemy Configuration
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Flask Mail Configuration
    MAIL_SERVER = os.getenv("MAIL_SERVER") or "smtp.sendgrid.net"
    MAIL_PORT = int(os.getenv("MAIL_PORT") or 587)
    MAIL_USE_TLS = bool(os.getenv("MAIL_USE_TLS")) or True
    MAIL_USE_SSL = bool(os.getenv("MAIL_USE_SSL")) or False
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
    MAIL_DEVELOPER = os.getenv("MAIL_DEVELOPER")

    # Flask Security Configuration
    SECURITY_PASSWORD_SALT = os.getenv("SECURITY_PASSWORD_SALT")
    SECURITY_TOKEN_MAX_AGE = 3600
    SECURITY_CONFIRM_SALT = os.getenv("SECURITY_CONFIRM_SALT")
    SECURITY_RESET_SALT = os.getenv("SECURITY_RESET_SALT")
    SECURITY_LOGIN_SALT = os.getenv("SECURITY_LOGIN_SALT")
    SECURITY_REMEMBER_SALT = os.getenv("SECURITY_REMEMBER_SALT")

    # send email when application error
    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        import logging
        from logging.handlers import SMTPHandler

        credentials = None
        secure = None
        if getattr(cls, "MAIL_USERNAME", None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)
            if getattr(cls, "MAIL_USE_TLS", None):
                secure = ()
        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr=cls.MAIL_DEFAULT_SENDER,
            toaddrs=cls.MAIL_ADMIN,
            subject=cls.APP_NAME + " - Application Error!",
            credentials=credentials,
            secure=secure,
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)


class HerokuConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        import logging
        from logging import StreamHandler

        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        from werkzeug.middleware.proxy_fix import ProxyFix

        app.wsgi_app = ProxyFix(app.wsgi_app)


ENV_CONFIG_DICT = dict(
    development=DevelopmentConfig,
    testing=TestingConfig,
    production=ProductionConfig,
    heroku=HerokuConfig,
)


def get_config(config_name):
    """Retrieve environment configuration settings."""
    return ENV_CONFIG_DICT.get(config_name, ProductionConfig)
