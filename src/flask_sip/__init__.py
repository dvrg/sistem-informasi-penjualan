import os
import logging
import sentry_sdk
from flask import Flask, render_template, current_app, url_for
from flask.helpers import get_root_path
from flask_security import Security, SQLAlchemyUserDatastore
from sentry_sdk.integrations.flask import FlaskIntegration
from flask_security import login_required
from logging.handlers import SMTPHandler, RotatingFileHandler
from flask_babel import format_currency
from flask_breadcrumbs import Breadcrumbs
from flask_images import resized_img_src
from src.flask_sip.config import get_config
from src.flask_sip.mod_app_profile.models import ApplicationProfile
from src.flask_sip.mod_user.forms import ExtendedRegisterForm
from src.flask_sip.mod_role.models import Role
from src.flask_sip.mod_user.models import User
from src.flask_sip.extensions import (
    db,
    migrate,
    mail,
    bootstrap,
    moment,
    babel,
    toolbar,
    ma,
    images,
    talisman,
)


user_datastore = SQLAlchemyUserDatastore(db, User, Role)


def create_app(config_name):
    """function for create initial app"""
    app = Flask(__name__)
    app.config.from_object(get_config(config_name))

    sentry_sdk.init(
        dsn=app.config["SENTRY_DSN"],
        integrations=[FlaskIntegration()],
        traces_sample_rate=1.0,
    )

    if not app.debug and not app.testing:
        if app.config["MAIL_SERVER"]:
            auth = None
        if app.config["MAIL_USERNAME"] or app.config["MAIL_PASSWORD"]:
            auth = (app.config["MAIL_USERNAME"], app.config["MAIL_PASSWORD"])
        secure = None
        if app.config["MAIL_USE_TLS"]:
            secure = ()
        mail_handler = SMTPHandler(
            mailhost=(app.config["MAIL_SERVER"], app.config["MAIL_PORT"]),
            fromaddr="no-reply@" + app.config["MAIL_SERVER"],
            toaddrs=app.config["MAIL_DEVELOPER"],
            subject="System Failure on " + app.config["APP_NAME"],
            credentials=auth,
            secure=secure,
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

    if not os.path.exists("logs"):
        os.mkdir("logs", mode=777)

    file_handler = RotatingFileHandler(
        "logs/flask-base.log", maxBytes=10240, backupCount=10, mode=777
    )
    file_handler.setFormatter(
        logging.Formatter("%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]")
    )
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info("System Startup...")

    register_context_processos(app)
    register_jinja_filters(app)
    register_extension(app)
    register_blueprint(app)
    register_dashapps(app)

    return app


def register_context_processos(app):
    """Register context processor"""

    @app.context_processor
    def default_app_processor():
        """ Application profile information using in accross template """
        ap = ApplicationProfile.get()
        if ap:
            return dict(
                app_name=ap.application_profile_name,
                app_meta_title=ap.application_profile_meta_title,
                app_meta_description=ap.application_profile_meta_description,
                app_meta_keyword=ap.application_profile_meta_keywords,
                app_google_analytics=ap.application_profile_google_analytics,
                app_logo_path=resized_img_src(
                    ap.application_profile_logo_name, width=100, mode="crop", quality=50
                ),
                app_favicon=url_for("static", filename="img/favicon/old/favicon.ico"),
            )
        else:
            return dict(
                app_name=current_app.config["APP_NAME"],
                app_meta_title=current_app.config["APP_NAME"],
                app_meta_description=current_app.config["APP_DESCRIPTION"],
                app_meta_keyword=current_app.config["APP_KEYWORD"],
                app_google_analytics=current_app.config["APP_GOOGLE_ANALYTICS"],
                app_logo_path=url_for("static", filename="img/100x100.svg"),
                app_favicon=url_for("static", filename="img/favicon/old/favicon.ico"),
            )

    @app.context_processor
    def utility_processor():
        def format_price(number, currency="IDR"):
            """Format all price to IDR"""
            return format_currency(
                number,
                currency,
            )

        return dict(format_price=format_price)


def register_jinja_filters(app):
    """Register jinja2 custome filter"""


def register_extension(app):
    """Register all extensions"""
    db.init_app(app)
    with app.app_context():
        if db.engine.url.drivername == "sqlite":
            migrate.init_app(app, db, render_as_batch=True)
        else:
            migrate.init_app(app, db)

    mail.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)
    babel.init_app(app)
    toolbar.init_app(app)
    ma.init_app(app)
    images.init_app(app)
    Security(app, user_datastore, register_form=ExtendedRegisterForm)
    Breadcrumbs(app=app)
    talisman.init_app(app, content_security_policy=app.config["CSP"])


def page_not_found(e):
    """wide custome error page, 404"""
    return render_template("error/404.html"), 404


def internal_server_error(e):
    """wide custome error page,500"""
    return render_template("error/500.html"), 500


def register_blueprint(app):
    """Register all blueprint"""
    from src.flask_sip.mod_dashboard import bp as dashboard_bp
    from src.flask_sip.mod_auth import bp as auth_bp
    from src.flask_sip.mod_user import bp as user_bp
    from src.flask_sip.mod_role import bp as role_bp
    from src.flask_sip.mod_bussines_profile import bp as profile_bp
    from src.flask_sip.mod_app_profile import bp as app_profile_bp
    from src.flask_sip.mod_category import bp as app_category_bp
    from src.flask_sip.mod_product import bp as app_product_bp
    from src.flask_sip.mod_pos import bp as pos_bp
    from src.flask_sip.apis import bp as api_bp

    app.register_blueprint(dashboard_bp)
    app.register_blueprint(auth_bp, url_prefix="/auth")
    app.register_blueprint(user_bp, url_prefix="/user")
    app.register_blueprint(role_bp, url_prefix="/role")
    app.register_blueprint(profile_bp, url_prefix="/business-profile")
    app.register_blueprint(app_profile_bp, url_prefix="/application-profile")
    app.register_blueprint(app_category_bp, url_prefix="/category")
    app.register_blueprint(app_product_bp, url_prefix="/product")
    app.register_blueprint(pos_bp, url_prefix="/pos")
    app.register_blueprint(api_bp, url_prefix="/api/v1")

    app.register_error_handler(404, page_not_found)
    app.register_error_handler(500, internal_server_error)


def register_dashapps(app):
    import dash
    import dash_bootstrap_components as dbc
    from src.flask_sip.dashapp1.layout import layout
    from src.flask_sip.dashapp1.callbacks import register_callbacks, register_callbacks2

    # Meta tags for viewport responsiveness
    meta_viewport = {
        "name": "viewport",
        "content": "width=device-width, initial-scale=1, shrink-to-fit=no",
    }

    dashapp1 = dash.Dash(
        __name__,
        server=app,
        url_base_pathname="/report-visualization/",
        assets_folder=get_root_path(__name__) + "/dashboard/assets/",
        external_stylesheets=[dbc.themes.BOOTSTRAP],
        meta_tags=[meta_viewport],
    )

    with app.app_context():
        dashapp1.title = "Report Visualization"
        dashapp1.layout = layout
        register_callbacks(dashapp1)
        register_callbacks2(dashapp1)

    _protect_dashviews(dashapp1)


def _protect_dashviews(dashapp):
    for view_func in dashapp.server.view_functions:
        if view_func.startswith(dashapp.config.url_base_pathname):
            dashapp.server.view_functions[view_func] = login_required(
                dashapp.server.view_functions[view_func]
            )
