from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, URL, Email
from flask_wtf.file import FileField, FileAllowed
from flask_babel import lazy_gettext as _l


class CreateBusinessProfile(FlaskForm):
    name = StringField(_l("Company name *"), validators=[DataRequired(), Length(2, 124)])
    logo = FileField(
        _l("Company logo"),
        validators=[FileAllowed(["png", "jpg", "jpeg"], _l("Images Only"))],
    )
    address = TextAreaField(_l("Address"))
    sub_district = StringField(_l("Sub-district"), validators=[Length(0, 64)])
    district = StringField(_l("District"), validators=[Length(0, 64)])
    province = StringField(_l("Province"), validators=[Length(0, 64)])
    country = StringField(
        _l("State"),
        validators=[Length(0, 64)],
        render_kw={"readonly": "readonly", "value": "Indonesia"},
    )
    postal_code = StringField(_l("Postal code"), validators=[Length(0, 12)])
    phone_number = StringField(_l("Phone number"), validators=[Length(0, 18)])
    email = StringField(_l("Email"), validators=[Email(), Length(0, 120)])
    website = StringField(_l("Website"))
    instagram = StringField(_l("Instagram"))
    youtube = StringField(_l("Youtube"))
    twitter = StringField(_l("Twitter"))
    facebook = StringField(_l("Facebook"))
    submit = SubmitField(_l("Save"))
