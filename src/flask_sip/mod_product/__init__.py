from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_product", __name__, template_folder="product_templates")
default_breadcrumb_root(bp, ".")

from . import routes
