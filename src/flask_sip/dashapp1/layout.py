import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from flask_babel import _

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "15rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f8f8",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.H2(_("Dashboard"), className="display-5 text-secondary"),
        html.Hr(),
        html.P(_("Visualization of report"), className="lead text-secondary"),
        dbc.Nav(
            [
                dbc.NavLink(_("Dashboard"), href="/report-visualization/", active="exact"),
                dbc.NavLink(_("Back"), href="/", active="exact", external_link=True),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div(
    [
        html.H3(children=_("Report Data All of Time"), className="text-center mb-4"),
        html.Div(
            children=[
                html.Div(
                    children=[
                        dcc.Dropdown(
                            id="my-dropdown",
                            options=[
                                {"label": _("Filter by Quantity"), "value": "Qty"},
                                {"label": _("Filter by Income"), "value": "Income"},
                                {"label": _("Filter by Profit"), "value": "Profit"},
                                {
                                    "label": _("Filter by Transaction"),
                                    "value": "Transaction",
                                },
                            ],
                            value="Profit",
                        ),
                        dcc.Graph(id="my-graph"),
                    ],
                    className="col-8",
                ),
                html.Div(
                    children=[
                        dcc.Dropdown(
                            id="pie-chart-dropdown",
                            options=[
                                {"label": _("Filter by Product Name"), "value": "name"},
                                {
                                    "label": _("Filter by Product Categories"),
                                    "value": "categories",
                                },
                            ],
                            value="name",
                        ),
                        dcc.Graph(id="pie-chart"),
                    ],
                    className="col-4",
                ),
            ],
            className="row",
        ),
    ],
    id="page-content",
    style=CONTENT_STYLE,
)

layout = html.Div([dcc.Location(id="url"), sidebar, content])
