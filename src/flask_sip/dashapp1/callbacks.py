import pandas as pd
import plotly.express as px
from dash.dependencies import Input
from dash.dependencies import Output
from src.flask_sip.mod_pos.models import Sales, Items
from flask import url_for
from flask_babel import _


def register_callbacks(dashapp):
    @dashapp.callback(Output("my-graph", "figure"), [Input("my-dropdown", "value")])
    def update_graph(selected_dropdown_value):
        data = Sales.to_dict_transaction_report()
        year = []
        month = []
        day = []
        qty = []
        income = []
        profit = []
        transaction = []

        if data:
            for d in data:
                year.append(d.year)
                month.append(d.month)
                day.append(d.day)
                qty.append(d.qty)
                income.append(d.income)
                profit.append(d.profit)
                transaction.append(d.transaction)

            df_new = pd.DataFrame(
                {
                    "Year": year,
                    "Month": month,
                    "Day": day,
                    "Qty": qty,
                    "Income": income,
                    "Profit": profit,
                    "Transaction": transaction,
                }
            )

            fig2 = px.line(
                df_new,
                x="Day",
                y=selected_dropdown_value,
                color="Month",
                line_shape="spline",
                render_mode="svg",
                title=selected_dropdown_value + " Chart",
            )

            return fig2


def register_callbacks2(dashapp):
    @dashapp.callback(Output("pie-chart", "figure"), [Input("pie-chart-dropdown", "value")])
    def pie_chart(selected_dropdown_value):
        name = []
        sold = []

        if selected_dropdown_value == "name":
            data = Items.most_buy_product_by_name()
        elif selected_dropdown_value == "categories":
            data = Items.most_by_product_by_categories()

        if data:
            for d in data:
                name.append(d[0])
                sold.append(d[1])

            df_most_buy = pd.DataFrame({"Name": name, "Sold": sold})

            fig_pie_chart = px.pie(
                df_most_buy,
                values="Sold",
                names="Name",
                title=_("Most Buy Product By ") + selected_dropdown_value,
            )

            return fig_pie_chart


def register_callbacks3(dashapp):
    @dashapp.callback(Output("page-content", "children"), [Input("url", "pathname")])
    def render_page_content(pathname):
        if pathname == "/dashboard":
            return url_for("/dashboard")
        elif pathname == "/":
            return url_for("mod_dashboard.index")
