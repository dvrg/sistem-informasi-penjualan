from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_category", __name__, template_folder="category_templates")
default_breadcrumb_root(bp, ".")

from . import routes
