from flask import render_template
from . import bp
from .models import Role
from flask_security import login_required, roles_accepted
from flask_babel import _
from flask_breadcrumbs import register_breadcrumb


@bp.route("/")
@login_required
@roles_accepted("Developer")
@register_breadcrumb(bp, ".roles", _("View Roles"))
def roles():
    roles = Role.gets()
    return render_template("roles/roles.html", roles=roles, title=_("Role"))
