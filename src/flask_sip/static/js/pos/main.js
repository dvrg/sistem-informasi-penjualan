Vue.component("list-cart", {
  template: `
        <b-alert type="light" variant="info" show class="mb-2">
          <div class="d-flex justify-content-between align-items-center">
            <div>
              <b-form-group :label="data.product_name" class="mb-0"></b-form-group>
            </div>
            <!--  -->
            <div class="text-right">
              <div class="text-danger mb-1">{{ new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR', maximumSignificantDigits: 6 }).format(data.product_selling_price) }}</div>
              <b-button-group size="sm">
                <b-button class="btn-info" @click="changeqty('-')">-</b-button>
                <b-button variant="outline-secondary" disabled style="color: black;">{{ data.qty }}</b-button>
                <b-button class="btn-info" @click="changeqty('+')">+</b-button>
              </b-button-group>
            </div>
          </div>
        </b-alert>
        `,
  props: ["data"],
  methods: {
    changeqty(op) {
      if (op == "+") {
        if (this.data.qty < this.data.product_stock) {
          this.data.qty++;
        }
        this.$emit("update-cart", this.data);
      } else {
        let result = this.data.qty
        if (result == 1) {
          this.data.qty = 0
        } else {
          this.data.qty--
        }
        this.$emit("update-cart", this.data);
      }
    },
  },
});

Vue.component("alert", {
  template: `
          <b-alert variant="success" show>{{ message }}</b-alert>
        `,
  props: ["message"]
});

const baseUrl = '/api/v1/'
const options = {
  name: '_blank',
  specs: [
    'fullscreen=no',
    'titlebar=yes',
    'scrollbars=no'
  ],
  styles: [
    'https://unpkg.com/bootstrap/dist/css/bootstrap.min.css',
  ]
}

const app = new Vue({
  el: "#app",
  data: {
    navs: [],
    barang: [],
    carts: [],
    u_cart: 0,
    total: {
      item: 0,
      price: 0,
      paid: 0,
      money_return: 0,
      profit: 0,
    },
    message: '',
    showMessage: false,
    disableButton: false,
    searchData: null,
  },
  watch: {
    'carts.qty': function (val) {
      val.map(cart => {
        this.total.item = + cart.qty
        this.total.price = + cart.price
        this.total.profit = + cart.profit
      })
    }
  },
  computed: {
    money_return: function () {
      return this.total.money_return = this.total.price - this.total.paid
    },
    validation: function () {
      return this.total.paid >= this.total.price
    },
    filterData() {
      if (this.searchData) {
        return this.barang.product.filter(item => {
          return this.searchData
            .toLowerCase()
            .split(" ")
            .every(v => item.product_name.toLowerCase().includes(v));
        });
      } else {
        return this.barang.product;
      }
    }
  },
  methods: {
    formatCurrency(price) {
      return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        maximumSignificantDigits: 5,
      }).format(price);
    },
    updateTotal(carts) {
      let result = {
        item: 0,
        price: 0,
        profit: 0,
      }
      carts.map(cart => {
        result.item += cart.qty
        let price = cart.product_selling_price * cart.qty
        result.price += price
        let profit = price - (cart.product_purchase_price * cart.qty)
        result.profit += profit
      })
      this.total.item = result.item
      this.total.price = result.price
      this.total.profit = result.profit
    },
    addCart(barang) {
      if (this.carts.length > 0) {
        checkBarang = this.carts.findIndex(x => x.product_id == barang.product_id)
        if (checkBarang == -1) {
          barang.qty = 1;
          this.carts.push(barang);
        } else {
          this.carts.map((cart) => {
            if (cart.product_id == barang.product_id) {
              cart.qty++
            }
          });
        }
        this.u_cart++
      } else {
        barang.qty = 1;
        this.carts.push(barang);
      }
      this.updateTotal(this.carts)
      this.disableButton = true
    },
    updateCart(data) {
      if (data.qty == 0) {
        let index = this.carts.findIndex(x => x.product_id == data.product_id)
        this.carts.splice(index, 1)
      } else {
        this.carts.map((cart) => {
          if (cart.product_id == data.product_id) {
            cart.qty = data.qty;
          }
        });
      }
      this.u_cart++
      this.updateTotal(this.carts)
      if (this.carts == 0) {
        this.disableButton = false
      }
    },
    getProducts() {
      axios.get(baseUrl + 'product/')
        .then(response => {
          this.barang = response.data
        })
        .catch(error => {
          console.log(error)
        });
    },
    getCategory() {
      axios.get(baseUrl + 'category/')
        .then(response => {
          this.navs = response.data
        })
        .catch(error => {
          console.log(error)
        });
    },
    postSales(event) {
      event.preventDefault()
      let items = []
      let obj = {}
      for (let i of this.carts) {
        obj = { "product_id": i.product_id, "qty": i.qty, "product_purchase_price": i.product_purchase_price, "product_selling_price": i.product_selling_price }
        items.push(obj)
      }
      axios.post(baseUrl + 'sales/create', {
        "price_total": this.total.price,
        "qty_total": this.total.item,
        "profit_total": this.total.profit,
        "items": items
      })
        .then(response => {
          this.message = response.data.message
          this.showMessage = true
          this.print()
          this.carts = []
          this.total.item = null
          this.total.price = null
          this.total.paid = null
          this.total.profit = null
          this.total.money_return = null
          this.getProducts()
        })
        .catch(error => {
          console.log(error)
        });

    },
    print() {
      this.$htmlToPaper('printMe');
    }
  },
  created() {
    this.getProducts();
    this.getCategory();
  },
  delimiters: ['[{', '}]']
});

function updateClock() {
  var currentTime = new Date();

  var currentHours = currentTime.getHours();
  var currentMinutes = currentTime.getMinutes();
  var currentSeconds = currentTime.getSeconds();

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
  currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = (currentHours < 12) ? "AM" : "PM";

  // Convert the hours component to 12-hour format if needed
  currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = (currentHours == 0) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

  // Update the time display
  document.getElementById("clock").firstChild.nodeValue = currentTimeString;
}
