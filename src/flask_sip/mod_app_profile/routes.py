import os
from flask import render_template, flash
from flask.globals import current_app
from . import bp
from flask_security import login_required, roles_accepted
from src.flask_sip.helpers import unique_filename

from .forms import CreateApplicationProfile
from .models import ApplicationProfile
from flask_babel import _
from flask_breadcrumbs import register_breadcrumb


@bp.route("/", methods=["GET", "POST"])
@login_required
@roles_accepted("Developer")
@register_breadcrumb(bp, ".application_profile", _("Application Profile"))
def application_profile():
    """
    Create or update application profile
    """
    form = CreateApplicationProfile()
    data = ApplicationProfile.get()
    if data is None:
        if form.validate_on_submit():
            data = ApplicationProfile(
                application_profile_name=form.name.data,
                application_profile_meta_title=form.meta_title.data,
                application_profile_meta_description=form.meta_description.data,
                application_profile_meta_keywords=form.meta_keywords.data,
                application_profile_google_analytics=form.google_analytics.data,
            )
            if form.logo.data is not None:
                # create unique filename and save it
                filename = unique_filename(form.logo.data)
                data.application_profile_logo_name = filename
                # set path to upload, save the file, save path
                if not os.path.exists(current_app.config["IMAGE_UPLOAD_DIR"]):
                    os.mkdir(current_app.config["IMAGE_UPLOAD_DIR"])
                else:
                    path = os.path.join(current_app.config["IMAGE_UPLOAD_DIR"], filename)
                    form.logo.data.save(path)
                # save path image without basedir, because it will call in direct from static folder
                data.application_profile_logo_path = os.path.join(
                    current_app.config["IMAGE_UPLOAD"], filename
                )
            data.create()
            flash(
                _("Data {} successfully created").format(
                    data.application_profile_meta_title.title()
                ),
                "info",
            )
    else:
        if form.validate_on_submit():
            data.application_profile_name = form.name.data
            data.application_profile_meta_title = form.meta_title.data
            data.application_profile_meta_description = form.meta_description.data
            data.application_profile_meta_keywords = form.meta_keywords.data
            data.application_profile_google_analytics = form.google_analytics.data
            if form.logo.data is not None:
                # create unique filename and save it
                filename = unique_filename(form.logo.data)
                data.application_profile_logo_name = filename
                # set path to upload, save the file, save path
                if not os.path.exists(current_app.config["IMAGE_UPLOAD_DIR"]):
                    os.mkdir(current_app.config["IMAGE_UPLOAD_DIR"])
                else:
                    path_old_image = os.path.join(
                        current_app.config["IMAGE_UPLOAD_DIR"],
                        data.application_profile_logo_name,
                    )
                    if os.path.exists(path_old_image):
                        os.remove(path_old_image)
                    path = os.path.join(current_app.config["IMAGE_UPLOAD_DIR"], filename)
                    form.logo.data.save(path_old_image)
                # save path image without basedir, because it will call in direct from static folder
                data.application_profile_logo_path = os.path.join(
                    current_app.config["IMAGE_UPLOAD"], filename
                )
            data.update()
            flash(
                _("Data {} successfully updated").format(
                    data.application_profile_meta_title.title()
                ),
                "info",
            )

        form.name.data = data.application_profile_name
        form.meta_title.data = data.application_profile_meta_title
        form.meta_description.data = data.application_profile_meta_description
        form.meta_keywords.data = data.application_profile_meta_keywords
        form.google_analytics = data.application_profile_google_analytics

    return render_template("profile.html", form=form, title=_("Application Profile"), data=data)
