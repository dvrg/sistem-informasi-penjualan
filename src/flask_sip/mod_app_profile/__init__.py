from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_app_profile", __name__, template_folder="app_profile_templates")
default_breadcrumb_root(bp, ".")

from . import routes
