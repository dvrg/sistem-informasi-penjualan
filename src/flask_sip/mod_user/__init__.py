from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_user", __name__, template_folder="admin_templates")
default_breadcrumb_root(bp, ".")
from . import routes
