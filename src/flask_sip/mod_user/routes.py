from datetime import datetime
from flask import render_template, redirect, url_for, flash
from flask_security import (
    login_required,
    current_user,
    roles_accepted,
    SQLAlchemyUserDatastore,
)
from flask_security.utils import hash_password
from flask_babel import _
from src.flask_sip.helpers import (
    get_local_image,
)
from src.flask_sip.extensions import db
from . import bp
from .forms import (
    EditProfileForm,
    CreateUserForm,
    CreateUserForAdminForm,
    EditSelectedProfileForm,
    EditSelectedProfileForAdminForm,
    PasswordForm,
)
from src.flask_sip.mod_user.models import User
from src.flask_sip.mod_role.models import Role
from flask_breadcrumbs import register_breadcrumb

user_datastore = SQLAlchemyUserDatastore(db, User, Role)


@bp.route("/static/<filename>")
def display_image(filename):
    """
    Display image from localfile
    """
    filename = get_local_image(filename)
    return redirect(url_for("static", filename="uploads/" + filename))


@bp.before_request
def before_request():
    """
    function that call for update status login of user
    """
    if current_user.is_authenticated:
        current_user.last_seen = datetime.now()
        db.session.commit()


@bp.route("/")
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".users", _("View Users"))
def users():
    """
    Get All User data
    """
    if current_user.roles == ["Developer"]:
        users = User.gets()
    else:
        users = User.gets_for_user()
    return render_template("main/users/users.html", title=_("User data"), users=users)


@bp.route("/create", methods=["GET", "POST"])
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".create_user", _("Create User"))
def create_user():
    """
    Create new user data
    """
    # Get Current Directory
    # directory = os.path.join(os.getcwd(), current_app.config["UPLOAD_FOLDER"])
    if current_user.roles == ["Admin"]:
        form = CreateUserForAdminForm()
    else:
        form = CreateUserForm()
    if form.validate_on_submit():
        data = user_datastore.create_user(
            username=form.username.data,
            about_me=form.about_me.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            title=form.title.data,
            email=form.email.data,
            password=hash_password(form.password.data),
            gender=form.gender.data,
            bornday=form.bornday.data,
            contact=form.contact.data,
            id_card=form.id_card.data,
            address=form.address.data,
            account_name=form.account_name.data,
            account_number=form.account_number.data,
            bank_name=form.bank_name.data,
            branch=form.branch.data,
            tax_id=form.tax_id.data,
            active=True,
            confirmed_at=datetime.now(),
            updated_at=datetime.now(),
            created_at=datetime.now(),
        )
        if form.role.data:
            for role in form.role.data:
                user_datastore.add_role_to_user(data, role)
        # Photo Local Store
        # file = form.photo.data
        # if file and allowed_file(file.filename):
        #     filename = unique_filename_for_user(file, form.username.data)
        #     file.save(os.path.join(directory, filename))
        db.session.commit()
        flash(
            _("User with username {} successfully created".format(form.username.data)),
            "info",
        )
        return redirect(url_for(".users"))
    return render_template("main/users/user.html", title=_("Create user"), form=form, edit=False)


@bp.route("/<username>")
@login_required
def user(username):
    """
    Get User by username
    @param is username
    """
    user = User.get(username)
    posts = [
        {"author": user, "body": "Test Post 1"},
        {"author": user, "body": "Test Post 2"},
    ]
    return render_template("main/users/user.html", title=_("User Panel"), user=user, posts=posts)


@bp.route("/profile", methods=["GET", "POST"])
@login_required
@roles_accepted("Developer", "Admin", "Cashier")
def personal_profile():
    """
    Get profile user for current login user.
    """
    form = EditProfileForm(current_user.username, current_user.email)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        current_user.first_name = form.first_name.data
        current_user.last_name = form.last_name.data
        current_user.title = form.title.data
        current_user.email = form.email.data
        current_user.gender = form.gender.data
        current_user.bornday = form.bornday.data
        current_user.contact = form.contact.data
        current_user.id_card = form.id_card.data
        current_user.address = form.address.data
        current_user.account_name = form.account_name.data
        current_user.account_number = form.account_number.data
        current_user.bank_name = form.bank_name.data
        current_user.branch = form.branch.data
        current_user.tax_id = form.tax_id.data
        current_user.updated_at = datetime.now()

        for role in form.role.data:
            if role not in current_user.roles:
                user_datastore.add_role_to_user(current_user, role)

        for data_role in current_user.roles:
            if data_role not in form.role.data:
                user_datastore.remove_role_from_user(current_user, data_role)

        current_user.update()
        flash(
            _(
                "Data {} {} successfully updated".format(
                    form.first_name.data,
                    form.last_name.data,
                )
            ),
            "info",
        )
        return redirect(url_for(".personal_profile"))
    # else:
    #    flash(_("Failed update data. Check form error"), "danger")
    form.username.data = current_user.username
    form.about_me.data = current_user.about_me
    form.first_name.data = current_user.first_name
    form.last_name.data = current_user.last_name
    form.title.data = current_user.title
    form.email.data = current_user.email
    form.gender.data = current_user.gender
    form.bornday.data = current_user.bornday
    form.contact.data = current_user.contact
    form.id_card.data = current_user.id_card
    form.address.data = current_user.address
    form.account_name.data = current_user.account_name
    form.account_number.data = current_user.account_number
    form.bank_name.data = current_user.bank_name
    form.branch.data = current_user.branch
    form.tax_id.data = current_user.tax_id
    form.role.data = [role for role in current_user.roles]
    return render_template("main/profiles/profile.html", title=_("Edit User"), form=form)


@bp.route("/edit/<username>", methods=["GET", "POST"])
@login_required
@roles_accepted("Developer", "Admin")
@register_breadcrumb(bp, ".edit_user", _("Edit User"))
def edit_user(username):
    """
    @username parameter
    Edit user data by @username
    """
    # Get Current Directory
    # directory = os.path.join(os.getcwd(), current_app.config["UPLOAD_FOLDER"])
    if current_user.username == username:
        return redirect(url_for(".personal_profile"))
    elif User.query.filter_by(username=username).first().roles == ["Developer"]:
        flash(_("You do not have permission to view this resource."), "danger")
        return redirect(url_for(".users"))
    else:
        data = User.get(username)
        if current_user.roles == ["Admin"]:
            form = EditSelectedProfileForAdminForm(username, data.email)
        else:
            form = EditSelectedProfileForm(username, data.email)
        if form.validate_on_submit():
            data.username = form.username.data
            data.about_me = form.about_me.data
            data.first_name = form.first_name.data
            data.last_name = form.last_name.data
            data.title = form.title.data
            data.email = form.email.data
            data.gender = form.gender.data
            data.bornday = form.bornday.data
            data.contact = form.contact.data
            data.id_card = form.id_card.data
            data.address = form.address.data
            data.account_name = form.account_name.data
            data.account_number = form.account_number.data
            data.bank_name = form.bank_name.data
            data.branch = form.branch.data
            data.tax_id = form.tax_id.data
            data.updated_at = datetime.now()
            # Updating Photo To Local Store
            # file = form.photo.data
            # if file is not None:
            #    if file and allowed_file(file.filename):
            #        filename = unique_filename_for_user(file, form.username.data)
            #        if os.path.isfile(
            #            current_app.config["UPLOAD_FOLDER"]
            #            + get_name_image(data.username)
            #        ):
            #            delete_image(data.username)
            #            file.save(os.path.join(directory, filename))
            #        else:
            #            file.save(os.path.join(directory, filename))
            # if form.role.data:
            # if this user have role same as select in form

            for role in form.role.data:
                if role not in data.roles:
                    user_datastore.add_role_to_user(data, role)

            for data_role in data.roles:
                if data_role not in form.role.data:
                    user_datastore.remove_role_from_user(data, data_role)

            db.session.commit()
            flash(
                _(
                    "Data {} {} successfully updated".format(
                        form.first_name.data,
                        form.last_name.data,
                    )
                ),
                "info",
            )
            return redirect(url_for(".edit_user", username=username))
        form.username.data = data.username
        form.about_me.data = data.about_me
        form.first_name.data = data.first_name
        form.last_name.data = data.last_name
        form.title.data = data.title
        form.email.data = data.email
        form.gender.data = data.gender
        form.bornday.data = data.bornday
        form.contact.data = data.contact
        form.id_card.data = data.id_card
        form.address.data = data.address
        form.account_name.data = data.account_name
        form.account_number.data = data.account_number
        form.bank_name.data = data.bank_name
        form.branch.data = data.branch
        form.tax_id.data = data.tax_id
        form.role.data = [role for role in data.roles]
    return render_template(
        "main/users/user.html",
        title=_("Edit User"),
        user=data,
        form=form,
        username=username,
        edit=True,
    )


@bp.route("/delete/<username>", methods=["GET", "POST"])
@login_required
@roles_accepted("Developer", "Admin")
def delete_user(username):
    """
    @username parameter
    Delete user by @username parameter
    """
    check_user_exist = user_datastore.find_user(username=username)
    if check_user_exist:
        user_datastore.delete_user(check_user_exist)
        db.session.commit()
        flash(_("Data {} successfully deleted".format(username)), "info")
        return redirect(url_for(".users"))
    else:
        flash(_("Data not found").format(check_user_exist.username), "danger")
        return redirect(url_for(".users"))


@bp.route("/password", methods=["GET", "POST"])
@login_required
def password():
    """
    Change password current user
    """
    form = PasswordForm()
    if form.validate_on_submit():
        current_user.set_password(form.new_password.data)
        current_user.update()
        flash(_("Password successfully updated"), "info")
        return redirect(url_for(".password"))
    return render_template("main/password/password.html", title=_("Password"), form=form)


@bp.route("/activate/<string:username>", methods=["GET", "POST"])
@login_required
@roles_accepted("Developer", "Admin")
def activate_user(username):
    check_user_exist = user_datastore.find_user(username=username)
    if check_user_exist:
        if check_user_exist.active:
            user_datastore.deactivate_user(check_user_exist)
            db.session.commit()
        else:
            user_datastore.activate_user(check_user_exist)
            db.session.commit()
        flash(
            _("Status User {} sucessfully updated").format(check_user_exist.username),
            "info",
        )
        return redirect(url_for(".users"))
    else:
        flash(_("Data not found").format(check_user_exist.username), "danger")
        return redirect(url_for(".users"))
