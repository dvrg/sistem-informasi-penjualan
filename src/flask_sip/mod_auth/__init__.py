from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_auth", __name__, template_folder="auth_templates")
default_breadcrumb_root(bp, ".")

from src.flask_sip.mod_auth import routes
