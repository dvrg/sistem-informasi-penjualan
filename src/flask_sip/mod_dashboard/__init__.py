from flask import Blueprint, render_template
from flask_breadcrumbs import default_breadcrumb_root

bp = Blueprint("mod_dashboard", __name__, template_folder="back_office_templates")
default_breadcrumb_root(bp, ".")

from . import routes
