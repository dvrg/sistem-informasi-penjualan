import click
import os
from datetime import datetime
from flask import g
from flask_babel import get_locale
from src.flask_sip import create_app, user_datastore
from src.flask_sip.extensions import db
from src.flask_sip.mod_user.models import User
from src.flask_sip.mod_role.models import Role
from src.flask_sip.mod_category.models import ProductCategory
from src.flask_sip.mod_product.models import Product
from src.flask_sip.mod_bussines_profile.models import BusinessProfile
from src.flask_sip.mod_app_profile.models import ApplicationProfile
from src.flask_sip.mod_pos.models import Sales, Items

app = create_app(os.getenv("FLASK_ENV"))


@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
        "User": User,
        "Role": Role,
        "ProductCategory": ProductCategory,
        "Product": Product,
        "BusinessProfile": BusinessProfile,
        "ApplicationProfile": ApplicationProfile,
        "Sales": Sales,
        "Items": Items,
    }


@app.before_request
def before_request():
    """
    store local information to g object, and call in moment.js
    """
    g.locale = str(get_locale())


@app.before_first_request
def seed_roles():
    """ Insert Default Roles data only if Role table is empty. """
    user_datastore.find_or_create_role(
        name="Cashier",
        updated_at=datetime.now(),
        created_at=datetime.now(),
    )
    user_datastore.find_or_create_role(
        name="Admin",
        updated_at=datetime.now(),
        created_at=datetime.now(),
    )
    user_datastore.find_or_create_role(
        name="Developer",
        updated_at=datetime.now(),
        created_at=datetime.now(),
    )
    db.session.commit()


@app.cli.group()
def translate():
    """ Translation and localization commands. """
    pass


@translate.command()
@click.argument("lang")
def init(lang):
    """Initialize a new language."""
    if os.system("pybabel extract -F babel.cfg -k _l -o messages.pot ."):
        raise RuntimeError("extract command failed")
    if os.system("pybabel init -i messages.pot -d src/flask_sip/translations -l " + lang):
        raise RuntimeError("init command failed")
    os.remove("messages.pot")


@translate.command()
def update():
    """Update all languages."""
    if os.system("pybabel extract -F babel.cfg -k _l -o messages.pot ."):
        raise RuntimeError("extract command failed")
    if os.system("pybabel update -i messages.pot -d src/flask_sip/translations"):
        raise RuntimeError("update command failed")
    os.remove("messages.pot")


@translate.command()
def compile():
    """Compile all languages."""
    if os.system("pybabel compile -d src/flask_sip/translations"):
        raise RuntimeError("compile command failed")
