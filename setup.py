"""Installation script for flask-admin application."""
from pathlib import Path
from setuptools import setup, find_packages

DESCRIPTION = "Sistem informasi penjualan sederhana menggunakan Flask, Vue.js dan SQLite"
APP_ROOT = Path(__file__).parent
README = (APP_ROOT / "README.md").read_text()
AUTHOR = "David Rigan"
AUTHOR_EMAIL = "1547711-dvrg@users.noreply.gitlab.com"
PROJECT_URLS = {
    "Documentation": "https://gitlab.com/dvrg/sistem-informasi-penjualan/-/wikis",
    "Bug Tracker": "https://gitlab.com/dvrg/sistem-informasi-penjualan/-/issues",
    "Source Code": "https://gitlab.com/dvrg/sistem-informasi-penjualan",
}
INSTALL_REQUIRES = [
    "Flask==1.1.4",
    "Flask-Security",
    "Flask-Mail",
    "Flask-SQLAlchemy",
    "Flask-Migrate",
    "Flask-Babel",
    "Flask-Moment",
    "Flask-DebugToolbar",
    "Flask-Bootstrap",
    "Flask-Breadcrumbs",
    "Flask-Images",
    "flask-restx",
    "flask-marshmallow",
    "Flask-WTF",
    "PyJWT",
    "WTForms-SQLAlchemy",
    "sqlalchemy==1.3.23",
    "python-dateutil",
    "python-dotenv",
    "python-slugify ",
    "email-validator",
    "bcrypt",
    "cryptography",
    "dash",
    "dash-bootstrap-components",
    "marshmallow-sqlalchemy",
    "pandas",
    "numpy",
    "sentry-sdk[flask]",
    "WTForms-SQLAlchemy",
    "psycopg2",
    "flask-talisman",
    "gunicorn",
    "itsdangerous<2.0",
]
EXTRAS_REQUIRE = {
    "dev": [
        "black",
        "flake8",
        "pre-commit",
        "pydocstyle",
        "pytest",
        "pytest-black",
        "pytest-clarity",
        "pytest-dotenv",
        "pytest-flake8",
        "pytest-flask",
        "tox",
    ]
}
setup(
    name="flask-sip",
    description=DESCRIPTION,
    long_description=README,
    long_description_content_type="text/markdown",
    version="0.1",
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    license="MIT",
    url="https://gitlab.com/dvrg/sistem-informasi-penjualan",
    project_urls=PROJECT_URLS,
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    python_requires=">=3.7",
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
)
